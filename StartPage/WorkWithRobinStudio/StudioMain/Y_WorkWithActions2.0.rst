=================================
Работа с действиями
=================================

В данном разделе приведено подробное описание действий, наиболее сложных для понимания, и приведены кейсы в качестве примеров. 

-------------------------------------
Try-Catch (Группа Базовые действия)
-------------------------------------

**Try-Catch** - действие группы «Базовые действия». 
Действие предназначено для обработки исключений в выбранных действиях, т.е. если при выполнении действия из блока «Try» произошла ошибка, указанная в параметрах действия «Try-Catch», то робот перейдет к выполнению действий, указанных в блоке «Catch».


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Порядок работы с Try-Catch
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


1. Выбрать действие «Try-Catch» из группы действий «Базовые действия» и перетащить  на рабочую область.

      .. figure:: TryCatch/TryC1.png
         :alt:

2. В блок «Try» поместить действия, для которых будет выполнено исключение. В блок «Catch» поместить действия, которые будут выполнены, если возникнет исключение, т.е. какая-либо ситуация, когда действие выполнено некорректно, иными словами, возникнет ошибка. Если блок «Catch» не заполнен, то робот продолжит работу сценария, проигнорировав возникшие ошибки.
Пример заполнения:


      .. figure:: TryCatch/TryC2.png
         :alt:

Блоков «Сatch» может быть несколько, тогда для каждого блока будет создан параметр «Исключение для CATCH [Номер блока]». Это нужно, когда для каждой возможной ошибки есть свой вариант действия.
Пример:
Действие «Try-Catch» в сценарии: 

      .. figure:: TryCatch/TryC3.png
         :alt:

Заполненные параметры действия «Try-Catch»: 

      .. figure:: TryCatch/TryC4.png
         :alt:

Таким образом, если веб-элемент не будет найден, то робот выполнит действие из блока «Catch», а если возникнет неизвестная ошибка, то робот выполнит действие из блока «CATCH 2». 

3.  Заполнить параметры действия «Try-Catch».
Параметры заполняются в формате коллекции, представляющей собой список исключений, для обработки которых предназначена соответствующая последовательность Catch.
Доступно несколько типов ошибок.  Для выбора типа ошибки необходимо нажать на выбранный тип, а затем на стрелку рядом с ним. 


      .. figure:: TryCatch/TryC5.png
         :alt:

После перенесения всех необходимых типов ошибок, нажать кнопку «Сохранить». 

Пример заполненных параметров: 

      .. figure:: TryCatch/TryC6.png
         :alt:

Пример заполненных параметров для нескольких блоков «Catch».

      .. figure:: TryCatch/TryC7.png
         :alt:

**Блок Finally**

Блок Finally является не обязательным блоком. Действия, указанные в этом блоке, выполнятся вне зависимости от того, возникла ошибка или нет. Если в блоке Finally отсутствуют действия, то робот продолжит свою работу.

Пример заполнения блока Finally:

      .. figure:: TryCatch/TryC8.png
         :alt:


Выше были рассмотрены общие моменты работы с действием «Try-Catch». Рассмотрим кейс «Погода», где продемонстрирована работа этого действия.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Кейс  «Погода»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Задача:** Роботу необходимо зайти в поисковую систему, сделать запрос «Погода» и получить сегодняшнюю температуру, а затем сохранить ее в excel-файл.
Задача простая, но в ходе ее выполнения могут возникнуть ошибки.

**Решение:**

1. Поместим все действия сценария в блок «Try»,
2. В блок «Catch» действие «Сообщение», в котором робот сообщит, что возникла ошибка при работе с браузером.
3. В блок «CATCH 2» поместим действие «Сообщение», которое сообщит, что ошибка произошла в действии группы «Excel».
4. В блоке Finally будет информационное сообщение о том, что робот закончил работу.

Пример заполнения блока «Try»: 

      .. figure:: TryCatch/TryC9.png
         :alt:

Пример заполнения блока «Catch»:

      .. figure:: TryCatch/TryC10.png
         :alt:

5. Заполним параметры: Выберем типы ошибок, которые могут возникнуть во время работы с действиями группы «Браузеры» для блока «Catch». 

      .. figure:: TryCatch/TryC11.png
         :alt:

И типы ошибок, которые могут возникнуть в результате работы с действиями группы «Excel» из предложенных для блока «CATCH 2».

      .. figure:: TryCatch/TryC12.png
         :alt:


Результат: 

      .. figure:: TryCatch/TryC13.png
         :alt:


Затем на экране появилось сообщение из блока «Finally». 

      .. figure:: TryCatch/TryC14.png
         :alt:


При работе робота произошла ошибка, анализируя лог в студии, можно увидеть, как робот остановил работу с действиями в блоке «Try», перешел в блок «CATCH 2» и вывел на экран информационное сообщение.

      .. figure:: TryCatch/TryC15.png
         :alt:


Из-за наличия блока «Try-Cath» робот завершил свою работу без ошибок , несмотря на то, что при открытии файла возникла ошибка.


-------------------------------------
Распознавание документов (OCR)
-------------------------------------

В данном разделе пошагово расписана работа с действиями группы "Распознавание текста":

* Подгруппа Yandex OCR -  Извлечение текста из изображения; 
* Подгруппа «Tesseract OCR» - Найти страницу в PDF;
* Подгруппа «Tesseract OCR» - Получить текст из PDF;
* Подгруппа «Tesseract OCR» - Прочитать текст;
* Подгруппа «FlexiCapture» - Распознать. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Подгруппа Yandex OCR -  Извлечение текста из изображения  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Для настройки действия "Извлечение текста из изображения" необходимо выполнить следующие шаги: 

1. Перенести действие  «Yandex OCR»  на рабочую область студии. 

  .. figure:: OCR/YO1.png
     :alt:

2. Записать путь до нужного изобрадения в разделе "Параметры" в поле "Объект изображения". 

  .. figure:: OCR/YO2.png
     :alt:


3. Записать ключ авторизации API Яндекс.Облако в поле "Yandex API-ключ". 
Для получения этого ключа нужно выполнить следующие действия: 


* Перейти на сайт Yandex.Cloud по ссылке https://cloud.yandex.ru/
* Кликнуть по кнопке «Подключиться». 

  .. figure:: OCR/YO3.png
     :alt:

* Кликнуть по кнопке «Войти в аккаунт на Яндексе».

  .. figure:: OCR/YO4.png
     :alt:

* Выполнить вход, заполнив необходимые поля и нажав на кнопку «Войти». Или зарегистрироваться и нажать на кнопку «Создать ID».

  .. figure:: OCR/YO5.png
     :alt:

* Авторизоваться, заполнив все поля.

  .. figure:: OCR/YO6.png
     :alt:


* Кликнуть по кнопке «Создать». 

  .. figure:: OCR/YO7.png
     :alt:

* Кликнуть по кнопке «Активировать пробный период».

  .. figure:: OCR/YO8.png
     :alt:

* Заполнить все поля и кликнуть по кнопке «Активировать».

  .. figure:: OCR/YO9.png
     :alt:

* Нажать на строку с именем каталога, в котором нужно создать сервисный аккаунт.

  .. figure:: OCR/YO10.png
     :alt:

* Кликнуть по имени.

  .. figure:: OCR/YO11.png
     :alt:

* Выбрать вкладку «Сервисные аккаунты».

  .. figure:: OCR/YO12.png
     :alt:

* Нажать кнопку «Создать сервисный аккаунт».

  .. figure:: OCR/YO13.png
     :alt:

* Ввести имя сервисного аккаунта, нажать на кнопку «Добавить роль» и выбрать роль, например admin. Кликнуть по кнопке «Создать».

  .. figure:: OCR/YO14.png
     :alt:

* Кликнуть по созданному аккаунту.

  .. figure:: OCR/YO15.png
     :alt:

* Кликнуть по кнопке «Создать новый ключ».

  .. figure:: OCR/YO16.png
     :alt:

* Выбрать из списка «Создать API-ключ».

  .. figure:: OCR/YO17.png
     :alt:

* Кликнуть по кнопке «Создать».

  .. figure:: OCR/YO18.png
     :alt:

* Копировать секретный ключ.

  .. figure:: OCR/YO19.png
     :alt:

* Вставить скопированный ключ в параметр «Yandex API-ключ» и запустить робота.

  .. figure:: OCR/YO20.png
     :alt:


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Подгруппа «Tesseract OCR» - Найти страницу в PDF  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Для настройки действия "Найти страницу в PDF необходимо выполнить следующие шаги: 

1. Перенести действие «Найти страницу в PDF» на рабочую область.

  .. figure:: OCR/YO21.png
     :alt:

2. Заполнить параметры действия:
   
a. Параметр «Путь к файлу PDF» - указать путь до PDF-файла, в котором будет выполнен поиск страницы;
b. Параметр «Текст» - указать текст, который должна содержать искомая страница;
c. Параметр «Ожидаемые языки в PDF файле» - выбрать из выпадающего списка язык, который ожидается на искомой странице. 

Доступные языки:

* Русский язык;
* Английский язык;
* Русский и Английский язык;
  
Пример заполненных параметров:

  .. figure:: OCR/YO22.png
     :alt:

3. Результатом действия является коллекция, в которой хранятся номера страниц, на которых был найден искомый текст.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Подгруппа «Tesseract OCR» - Получить текст из PDF 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Перенести действие «Получить текст из PDF» на рабочую область.

  .. figure:: OCR/YO23.png
     :alt:

2. Заполнить параметры действия:
   
a. Параметр «Путь к файлу PDF» - указать путь до PDF-файла, в котором будет выполнен поиск страницы;

b. Параметр «Язык текста» - выбрать из выпадающего списка язык, который ожидается на искомой странице. 

Доступные языки:

* Русский язык;
* Английский язык;
* Русский и Английский язык;

Параметр «Номер страницы» - номер страницы файла, с которой будет считываться текст.
Пример заполненных параметров:

  .. figure:: OCR/YO24.png
     :alt:

3. Результат действия – текст с указанной страницы PDF-файла. Если страница отсутствует в документе, будет возвращено пустое значение.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Подгруппа «Tesseract OCR» - Прочитать текст 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Для настройки действия "Прочитать текст", предназначенного для распознавания и чтения текста с изображения, необходимо выполнить следующие шаги: 

1. Перенести действие «Прочитать текст» на рабочую область.

  .. figure:: OCR/YO25.png
     :alt:


2. Заполнить параметры действия:
   
a. Параметр «Изображение» - ссылка на изображение, с которой будет прочитан текст;

b. Параметр «Ожидаемые языки» -  выбрать из выпадающего списка язык, который ожидается на искомой странице. 

Доступные языки: 

* Русский язык;
* Английский язык;
* Русский и Английский язык;
  
c. Параметр «Формат контента» - выбрать из выпадающего списка ожидаемый формат текста. 

Доступные форматы:

* Строка;
* Блок;
* Страница.

Пример  заполнения параметров: 

  .. figure:: OCR/YO26.png
     :alt:

3. Результат действия – строка, содержащая текст, полученный из изображения. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Подгруппа «FlexiCapture» - Распознать
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Для настройки действия "Распознать" необходимо выполнить следующие шаги: 

1. Перенести на рабочую область действие «Распознать».

  .. figure:: OCR/YO27.png
     :alt:


2. Заполнить параметры действия:
   
a. Параметр «Графический файл» - указать путь до графического файла, из которого необходимо извлечь текст;
b. Параметр «Url к веб-серверу FlexiCapture» - указать ссылку для авторизации на веб-сервере FlexiCapture;
c. Параметр «Логин» - логин для авторизации на веб - сервере FlexiCapture;
d. Параметр «Пароль» - пароль для авторизации на веб - сервере FlexiCapture;
e. Параметр «ID роли подключения» - указать идентификатор роли оператора FlexiCapture;
f. Параметр «ID типа станции» - указать идентификатор типа станции FlexiCapture;
g. Параметр «Имя или Guid проекта» - указать имя или идентификатор проекта FlexiCapture;
h. Параметр «ID типа пакета» - указать идентификатор типа пакета FlexiCapture;
i. Параметр «ID пользователя или группы» - указать идентификатор владельца пакета FlexiCapture;
j. Параметр «ID стадии экспорта» - указать идентификатор стадии экспорта FlexiCapture;
k. Параметр «Формат вложения» - выбрать из выпадающего списка расширение файла, который будет возвращен с сервера. 


Доступны следующие типы:

* xlsx;
* xls;
* xml;
* json;
* csv;
* txt;
* dbf.
  
l. Параметр «Папка для вложения» - казать путь до папки, куда будет сохранено вложение полученное с сервера.

Пример заполнения полей: 

  .. figure:: OCR/YO28.png
     :alt:


3. Результат действия – словарь с результатом распознавания в формате ключ-значение и путь до файла с результатом распознавания.


--------------------------------
REST и SOAP запросы
--------------------------------

В данном описании рассмотрены способы заполнения параметров действий REST и SOAP для понимания работы с запросами при помощи Robin Robot, включая общую информацию по данным запросам.

Также краткое описание к каждому действию есть в студии, оно расположено на самом значке действия во всплывающем знаке вопроса, рассмотреть его можно нажав на знак вопроса, а затем перейдя по подсказке «Подробнее».


      .. figure:: SOAPREST/SR.png
         :alt:


**Отправить запрос / Send request**

Группа действий: Интеграции

Подгруппа: REST

      .. figure:: SOAPREST/SR1.png
         :alt:


Действие «Отправить запрос» – предназначено для отправки HTTP-запроса для обращения к RESTful веб-сервису.

REST-запрос – это архитектурный стиль создания web-сервиса, когда на имеющиеся URL можно отправлять запросы для получения информации.

В таблице ниже приведено описание параметров действия. 

      .. figure:: SOAPREST/SR2.png
         :alt:

Описание параметров: тело запроса, параметры запроса, параметры формы, авторизация, заголовки запроса.  

      .. figure:: SOAPREST/SR3.png
         :alt:
       

Описание параметра "прикрепить файл".

      .. figure:: SOAPREST/SR4.png
         :alt:


**Запрос / Request**

Группа действий: Интеграции

Подгруппа: SOAP

      .. figure:: SOAPREST/SR5.png
         :alt:

SOAP (HTTP-запрос для обращения к веб-сервису (SOAP).

SOAP – это простой протокол, который используется для обмена произвольными сообщениями в формате XML, а также для реализации удалённого вызова процедур.

 

В таблице ниже приведено описание параметров действия.

      .. figure:: SOAPREST/SR6.png
         :alt:


--------------------------------
Циклы 
--------------------------------


Циклы необходимы для повторения определенной последовательности действий нужное количество раз.

В ROBIN Studio 2.0 представлено четыре вида циклов в группе действий «Базовые действия»: «Для», «Для каждого», «Пока» и «Цикл с постусловием», а так же действия «Прервать цикл» и «Продолжить цикл» для остановки работы цикла и перехода к началу его выполнения. 

  .. figure:: Loops/ЦИ1.png
         :alt:

Рассмотрим порядок работы с каждым циклом.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Цикл «Для»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Данное действие предназначено для обеспечения заданной итерации по счетчику шагов, т.е. цикл выполнит действия внутри него заданное количество раз.

*Порядок работы с действием цикл «Для»:*

1.Выбрать действие «Для» из группы действий «Базовые действия» и перенести в сценарий;

  .. figure:: Loops/ЦИ2.png
         :alt:


2. Заполнить тело цикла необходимыми действиями;
Пример заполнения: 

  .. figure:: Loops/ЦИ3.png
         :alt:

3. Заполнить параметры действия:

* Начальное значение (значение, с которого начнется отсчет);
* Конечное значение (значение до которого будет вестись отсчет.);
* Инкремент (число, на которое будет увеличиваться значение итератора);
  
Пример заполнения: 

 .. figure:: Loops/ЦИ4.png
         :alt:

4. Результат работы цикла: состояние итератора, т.е. значение счетчика для использования в текущей итерации цикла.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Цикл «Для каждого»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Данный цикл предназначен для последовательной работы с каждым элементом выбранной коллекции.

*Порядок работы с циклом «Для каждого»:*

1.Выбрать действие «Для каждого» из группы действий «Базовые действия» и перенести в сценарий; 

 .. figure:: Loops/ЦИ5.png
         :alt:

2. Заполнить тело цикла необходимыми действиями;
Пример заполнения:

 .. figure:: Loops/ЦИ6.png
         :alt:

3. Заполнить параметр действия:

* Коллекция (список значений, которые будут обработаны в цикле)
  
Пример заполнения параметра: 

 .. figure:: Loops/ЦИ7.png
         :alt:

4. Результат работы цикла: объект, в который будет помещен каждый элемент коллекции во время выполнения итерации.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Сравнение циклов «Для» и «Для каждого»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Цикл «Для» выполнит работу заданное количество раз. Итератор будет принимать значения от начального заданного значения до конечного с шагом, указанным в поле «Инкремент».
Цикл «Для каждого» последовательно работает с элементами выбранной коллекции. Количество итераций равно количеству элементов в коллекции, выбранной на входе.
Таким образом результат цикла «Для» это число, а «Для каждого» объект.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Цикл «Пока»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Данное действие предназначено для создания цикла действий, который выполняется до тех пор, пока результат условия равен "True".

*Порядок работы с циклом «Пока»:* 

1. Выбрать действие «Пока» из группы действий «Базовые действия» и перенести в сценарий.

 .. figure:: Loops/ЦИ8.png
         :alt:


2. Заполнить тело цикла (блок внутри цикла, где прописывается алгоритм для работы робота) необходимыми действиями;
Пример заполнения: 

 .. figure:: Loops/ЦИ9.png
         :alt:


3. Заполнить параметр действия:

* Условие (критерий, по которому будет приниматься решение о продолжении или прекращении работы цикла).
Пример заполнения: 

 .. figure:: Loops/ЦИ10.png
         :alt:

Если условие заполняется посредством инструмента построения выражений, то оно отобразиться в разделе «Условие» внутри цикла.
Инструмент построения выражений:

 .. figure:: Loops/ЦИ11.png
         :alt:

При заполнении чек-бокса условие цикла считается равным True. Если чек-бокс не заполнен и не указано иное условие, то условие цикла считается равным False.

Пример цикла с условием, заполненным с помощью инструмента построения выражений: 

 .. figure:: Loops/ЦИ12.png
         :alt:

   

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
«Цикл с постусловием»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Данное действие предназначено для создания цикла действий, который выполняется до тех пор, пока результат условия равен "True"

Порядок работы с «Циклом с постусловием»: 

1. Выбрать действие «Цикл с постусловием» из группы действий «Базовые действия» и перенести в сценарий.


 .. figure:: Loops/ЦИ13.png
         :alt:


2. Заполнить тело цикла необходимыми действиями.
Пример заполнения:

 .. figure:: Loops/ЦИ14.png
         :alt:


3. Заполнить параметр действия.

* Условие
Пример заполнения:

 .. figure:: Loops/ЦИ15.png
         :alt:

Если условие заполняется посредством инструмента построения выражений, то оно отобразиться в разделе «Условие» внутри цикла.
Пример цикла с постусловием, где условие заполнено с помощью инструмента построения выражений:

 .. figure:: Loops/ЦИ16.png
         :alt:



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Сравнение циклов «Пока» и цикла с «Постусловием»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Оба цикла работают до тех пор, пока результат условия равен "True". Однако в цикле «Пока» проверка действия происходит перед началом выполнения цикла, т.е. цикл может отработать 0 раз и более.
Проверка условия в действии «Цикл с постусловием» происходит после выполнения действий в теле цикла, т.е. цикл отработает минимум один раз.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Действия "Прервать цикл" и "Продолжить цикл"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Действия «Прервать цикла» и «Продолжить цикл» находятся в группе действий «Базовые действия» в подгруппе «Циклы».

*Действие «Прервать цикл»*

Выбрать действие «Прервать цикл» и перенести в сценарий. 

 .. figure:: Loops/ЦИ17.png
         :alt:


На этом этапе сценария робот завершит работу с циклом и продолжит выполнять действий вне цикла.

*Действие «Продолжить цикл»*

Выбрать действие «Продолжить цикл» и перенести в сценарий.

 .. figure:: Loops/ЦИ18.png
         :alt:

На этом этапе сценария робот завершит работу с циклом и начнет выполнять действия в теле цикла сначала.



Для более точного понимания работы циклов в Robin Studio рассмотрим небольшие кейсы.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Кейс «Нумерация строк». Цикл «Для»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Задача:* пронумеровать строки в excel-файле.

*Решение:*

1. Создать переменную «Номер». Тип переменной «Число».

 .. figure:: Loops/ЦИ19.png
         :alt:

2. Перенести на рабочую область действие «Открыть» из группы действий «Excel».

3. Перенести на рабочую область действие «Для» (цикл) из группы действий «Базовые действия».

4. Перенести на рабочую область действия «Сохранить» из группы действий «Excel».

5. Перенести на рабочую область действия «Закрыть» из группы действий «Excel».

 .. figure:: Loops/ЦИ20.png
         :alt:

6. Заполнить параметры действий, перенесенных на рабочую область:

а. Действие «Открыть»: В строке «Путь к фалу» указать путь к excel-файлу в котором будет проведена работа. 

.. figure:: Loops/ЦИ21.png
         :alt:

b. Действие «Для»:
«Начальное значение», «Конечное значение» и «Инкремент».
В параметр «Результат» записать созданную ранее переменную «Номер».

Пример заполнения параметров:

.. figure:: Loops/ЦИ22.png
         :alt:

с. Для действия «Сохранить». 

В строке «Контекст» указать экземпляр открытого excel-файла.

.. figure:: Loops/ЦИ23.png
         :alt:

d. Действие «Закрыть». 
В строке «Контекст» указать экземпляр открытого excel-файла. 

.. figure:: Loops/ЦИ24.png
         :alt:

7. В тело цикла поместить действия «Конвертировать данные» из группы «Базовые действий», «Соединить» из группы «Текст» и «Установить значение ячейки» из группы действий «Excel». 

.. figure:: Loops/ЦИ25.png
         :alt:

8. Заполнить параметры выбранных действий в теле цикла:

a. Действие «Конвертировать данные» 

* В строке «Источник» выбрать переменную «Номер».
* В строке «Тип» выбрать значение «Строка» из выпадающего цикла.

.. figure:: Loops/ЦИ26.png
         :alt:

b. Действие «Соединить».

* «Текст 1»  - название столбца, который будет пронумерован.
* «Текст 2» - результат действия «Конвертировать данные».

.. figure:: Loops/ЦИ27.png
         :alt:

с. Действие «Установить значение ячейки».

* В строке «Контекст» указать экземпляр открытого excel-файла.
* В строке «Ячейка» указать результат действия «Соединить».
* В строке «Значение» указать  переменную «Номер». 

.. figure:: Loops/ЦИ28.png
         :alt:

9. Запустить робота по кнопке "Запуск" в верхней панели.

.. figure:: Loops/ЦИ29.png
         :alt:

*Результат:* алгоритм прошел по циклу, ячейки в выбранном столбце excel-файла были пронумерованы.



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Кейс «Сортировка». Цикл «Для каждого»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Задача:* В папке находятся файлы с расширением .xls(x) и .docx. Необходимо создать папки с названием расширения и распределить файлы по папкам.

*Решение:*

1. На рабочую область перенести действия «Получить содержимое папки» и «Для каждого». 

.. figure:: Loops/ЦИ30.png
         :alt:

2.  Заполнить параметры действий.

а. Действие «Получить содержимое папки».

* В строке «Папка» указать путь до папки с файлами, которые необходимо рассортировать.
* В поле  «Тип объектов» указать значение  "только файлы". 

.. figure:: Loops/ЦИ31.png
         :alt:

b. Действие «Для каждого». 

* В строке «Коллекция» указать результат выполнения действия «Получить содержимое папки»
Пример:

.. figure:: Loops/ЦИ32.png
         :alt:

3. В тело цикла поместить следующие действия в указанном порядке:

a. «Получить информацию о файле»;
b. «Извлечь с позиции»;
c. «Найти папку»;
d. «Получить длину»;
e. «Если»
f. На ветвь «Истина» поставить действия:
  i. «Получить значение по индексу»;
  ii. Конвертировать данные; 
  iii. «Переместить файл»; 
g. На ветвь «Ложь» установить действия:
  i.  «Создать новую папку»;
  ii. «Переместить файл»;

.. figure:: Loops/ЦИ33.png
         :alt:


4. Заполнить параметры действий.

a. Для действия «Получить информацию о файле»:

* Параметр «Файл» - ссылка на объект, обрабатываемый в момент итерации цикла.
* Параметр «Свойство» - «Тип».

.. figure:: Loops/ЦИ34.png
         :alt:

b. Для действия «Извлечь с позиции»;

* Параметр «Исходный текст» - результат действия «Получить информацию о файле».
* Параметр «Позиция» - 1.
* Параметр «Длина» - если данный параметр не заполнен, то текст извлекается с заданной позицией до конца.

.. figure:: Loops/ЦИ35.png
         :alt:


с. Для действия «Найти папку». 

* Параметр «Папка для поиска» - путь до папки, где храниться подпапка для сортировки файлов.
* Параметр «Шаблон поиска» - результат действия «Извлечь с позиции».

.. figure:: Loops/ЦИ36.png
         :alt:

d. Для действия «Получить длину». 
Параметр «Коллекция или массив» - результат действия «Найти папку». 

.. figure:: Loops/ЦИ37.png
         :alt:

e. Для действия «Если». 
С помощью инструмента построения выражения в параметр условия записать логический оператор «Больше чем», где первый операнд – результат действия «Получить длину», а второй – 0.

.. figure:: Loops/ЦИ38.png
         :alt:

f. Для действия «Получить значение по индексу».

* Параметр «Коллекция» - результат действия «Найти папку».
* Параметр «Индекс» - 0. 

.. figure:: Loops/ЦИ39.png
         :alt:


g. Для действия «Конвертировать данные». 

* Параметр «Источник» - результат действия «Путь к папке». 
* Параметр «Тип» - путь к папке. 

.. figure:: Loops/ЦИ40.png
         :alt:

h.  Для действия «Переместить файл».

* Параметр «Файл» - ссылка на объект, обрабатываемый в момент итерации цикла.
* Параметр «Папка назначения» - результат действия «Конвертировать данные».

.. figure:: Loops/ЦИ41.png
         :alt:

j. Для действия «Создать новую папку». 

* Параметр «Родительская папка» - путь до папки, где , где храниться подпапка для сортировки файлов.
* Параметр «Имя» - результат действия «Извлечь с позиции».

.. figure:: Loops/ЦИ42.png
         :alt:

k. Для действия «Переместить файл».

* Параметр «Файл» - ссылка на объект, обрабатываемый в момент итерации цикла.
* Параметр «Папка назначения» - результат действия «Создать новую папку».

.. figure:: Loops/ЦИ43.png
         :alt:

*Результат:* Алгоритм прошел по циклу, рассортировывая файлы по папкам.



~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Кейс «Первое вхождение». Цикл «Пока»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Задача:* В excel-файле записаны дата и температура за это число. Необходимо найти первый день в списке, когда температура стала отрицательной.

*Решение:*

1. Создать переменные «Счетчик» и «Значение» тип переменных – число. 

.. figure:: Loops/ЦИ44.png
         :alt:

2. На рабочую область перенести действия «Открыть» и «Получить данные из столбца» (группа «Excel»), действие «Пока» (группа «Базовые действия»), «Сохранить» и «Закрыть» (группа «Excel»). 

.. figure:: Loops/ЦИ45.png
         :alt:

3.  Заполнить параметры действий:

а. Действие «Открыть»: в строке «Путь к фалу» указать путь к excel-файлу с котором будет проведена работа.

.. figure:: Loops/ЦИ46.png
         :alt:

b. Действие «Получить значение из столбца». 
Указать контекст и название столбца с температурными данными.

.. figure:: Loops/ЦИ47.png
         :alt:

с. Действие «Пока». 
С помощью инструмента построения выражений записать условие «Переменная Значение больше 0». 
Пример: 

.. figure:: Loops/ЦИ48.png
         :alt:

d. Действие «Сохранить».
В строке «Контекст» указать экземпляр открытого excel-файла.

.. figure:: Loops/ЦИ49.png
         :alt:

е. Действие «Закрыть». 
В строке «Контекст» указать экземпляр открытого excel-файла. 

.. figure:: Loops/ЦИ50.png
         :alt:

4. В тело цикла добавить действие «Получить значение по индексу» (группа действий «Коллекции») и «Сложение» (группа действий «Операторы»).

.. figure:: Loops/ЦИ51.png
         :alt:

5. Заполнить параметры этих действий:

а. Действие «Получить значение».  

* В строке коллекция указать результат действия «Получить значения из столбца».
* В строке «Индекс» указать переменную «Счетчик».
* В параметре «Результат» записать переменную «Значение».

.. figure:: Loops/ЦИ52.png
         :alt:

b. Действие «Сложение».

* Первый слагаемое – переменная «Счетчик».
* Второе слагаемое – 1.
* Результат – переменная «Счетчик».

.. figure:: Loops/ЦИ53.png
         :alt:

*Результат:* алгоритм начнет перебирать значения коллекции в цикле и прекратит работу, как только встретит первое отрицательное число. Оно будет записано в переменную «Значение».


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Кейс «Поиск файла». «Цикл с постусловием»
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Задача:* В папке есть несколько подпапок, где лежит необходимый нам файл. Нужно просмотреть подпапки и выяснить в какой из них сохранен этот файл.

*Решение:*

1. Создать переменную «Счетчик», тип переменной – число.

2. Создать переменную «Значение», тип переменной – объект.

.. figure:: Loops/ЦИ54.png
         :alt:

3. На рабочую область перенести действия «Получить содержимое папки» (группа действий «Файловая система»), «Цикл с постусловием» (группа действий «Базовые действия»).

.. figure:: Loops/ЦИ55.png
         :alt:

4. Заполнить параметры действий:

а. Для действия «Получить содержимое папки»:

* В параметр «Папка» указать путь до папки с подпапками.
* В параметре «Тип объектов» выбрать из выпадающего списка значение «Только папки».

.. figure:: Loops/ЦИ56.png
         :alt:

b. Для действия «Цикл с постусловием». 
В параметр условие с помощью инструмента построение выражений записать условие «Длинна коллекции, полученной в результате действия «Найти файл» равно 0»
Пример:

.. figure:: Loops/ЦИ57.png
         :alt:

5.  В тело цикла добавить действия «Получить значение по индексу» (группа действий «Коллекции»), «Конвертировать данные» (группа действий «Базовые действия»), «Найти файл» (группа действий «Файловая система») и «Сложение» (группа действий «Операторы»).
   
.. figure:: Loops/ЦИ58.png
         :alt:

6.  Заполнить параметры действий:

а. Для действия «Получить значение по индексу»:

* Параметр «Коллекция» - результат действия «Получить содержимое папки».
* Параметр «Индекс» - переменная «Счетчик».
* Параметр «Результат» - переменная «Значение».

.. figure:: Loops/ЦИ59.png
         :alt:


b. Для действия «Конвертировать данные»:

* Параметр «Источник» - результат действия «Получить значение по индексу».
* Параметр «Тип» - путь до папки.

.. figure:: Loops/ЦИ60.png
         :alt:

с. Для действия «Найти файл»: 

* Параметр «Папка для поиска» - результат действия «Конвертировать данные».
* Параметр «Шаблон» - название искомого файла.

.. figure:: Loops/ЦИ61.png
         :alt:

d. Для действия «Сложение»: 

* Первое слагаемое – переменная «Счетчик».
* Второе слагаемое – 1.
* Результат – переменная «Счетчик».

.. figure:: Loops/ЦИ62.png
         :alt:

*Результат:* алгоритм пройдет по циклу до тех пор, пока не будет найден искомый файл. Путь до папки с этим файлом будет сохранен в переменную «Значение». 


-------------------
Базы данных  
-------------------


Работу с базами данных робот осуществляет посредством действий группы «Базы данных».  
Группа действий содержит три подгруппы. Действия подгруппа «Запросы» позволяет обрабатывать базу данных, действия подгруппы «Общее» организует настройку подключения к базе данных и закрытие подключения с ней, действия подгруппы «Транзакции» позволяют закрыть и откатить транзакцию.

При создании сценария робота, работающего с базой данных, сначала необходимо настроить подключение к ней.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Подключение к базе данных
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

В Robin Studio существует два типа подключения к базе данных: пользовательское и стандартное.

**Настройка стандартного подключения:**

1. Выбрать действие «Стандартное подключение».

      .. figure:: БД/БД1.png
         :alt:


2. Заполнить параметры действия.
* Тип СУБД (Обязательное поле). 
  
Доступно четыре типа СУБД:
Oracle
MySQL
PostgreSQL
MsSqlServer

* Логин (Обязательное поле);
* Пароль;
* Хост сервера;
* Порт сервера;
* Имя БД;
* Тайм-аут;
* Уровень изоляции;
* Параметры.


Пример заполнения параметров:  

      .. figure:: БД/БД2.png
         :alt:

3. Перейти к созданию сценария робота. При запуске робот подключится к выбранной СУБД.  

**Настройка пользовательского подключения:**

1. Выбрать действие «Пользовательское подключение». 

  .. figure:: БД/БД3.png
         :alt:

2. Заполнить параметры действия:
* Выбрать уровень изоляции из выпадающего списка. Доступно пять уровней изоляции:
Без транзакций;
Чтение незаконченных транзакций разрешено;
Повторное чтение данных вернет те же значения, что и в начале транзакции;
Сериализуемость.

* Логин;
* Пароль;
* URL сервера, где размещена БД (обязательное поле);
* Класс драйвера (обязательное поле);
* Путь к классу драйвера (обязательное поле);
* Тайм-аут.

Пример заполнения параметров: 

  .. figure:: БД/БД4.png
         :alt:

3. Перейти к созданию сценария робота. При запуске робот подключится к выбранной СУБД. По завершению работы с базой данных роботу необходимо закрыть подключение к ней.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Закрытие подключения к выбранной базе данных
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


1. Выбрать действие «Закрыть подключение». 


  .. figure:: БД/БД5.png
         :alt: 

2. Заполнить параметры действия: 
* Контекст БД (Контекст подключения к выбранной базе данных).
  
Пример заполнения:

  .. figure:: БД/БД6.png
         :alt: 

В Robin Studio работу с базой данных можно организовать посредством следующих действий: 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Выполнение запроса 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Запрос в базу данных выполняется на языке запросов SQL.

1. Выбрать действие «Выполнить запрос».

  .. figure:: БД/БД7.png
         :alt: 


2. Заполнить параметры:
* Контекст БД (обязательное поле);
* Шаблон запроса;
* Список параметров.
  
Пример заполнения запроса без параметров: В поле «Шаблон запроса» вводится запрос на языке SQL. 

  .. figure:: БД/БД8.png
         :alt: 

Пример заполнения запроса с параметром.
В поле «Шаблон запроса» вводится запрос на языке SQL, где в месте, куда необходимо будет подставить параметр, знак вопроса. 
Список параметров оформляется в формате словаря в том порядке, в котором они будут заполняться в шаблоне. 

  .. figure:: БД/БД9.png
         :alt: 

Результат действия - таблица. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Добавление записи  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Выбрать действие «Добавить запись». 

  .. figure:: БД/БД10.png
         :alt:

2. Заполнить параметры действия:
* Контекст БД (обязательное поле);
* Имя таблицы (обязательное поле);
* Данные для вставки (обязательное поле); 
  
Поле заполняется в формате словаря. 
Пример заполнения: 

  .. figure:: БД/БД11.png
         :alt:

* Список имен полей первичного ключа.

Пример заполнения параметров: 

  .. figure:: БД/БД12.png
         :alt:

Результат действия - таблица. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Извлечение записи из базы данных
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Выбрать действие «Извлечь запись». 

  .. figure:: БД/БД13.png
         :alt:

2. Заполнить параметры действия:
* Контекст БД (обязательное поле);
* Имя таблицы (обязательное поле);
* Поля таблицы. Заполняются в формате коллекции;
* Значения первичного ключа. Заполняется в формате словаря (обязательное поле).
  
Пример заполнения полей:

  .. figure:: БД/БД14.png
         :alt:

Результат действия: словарь, где ключи – указанные в запросе имена столбцов. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Обновление записи в базе данных
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Выбрать действие «Обновить запись». 
 
  .. figure:: БД/БД15.png
         :alt:

2. Заполнить параметры действия. Все поля обязательны для заполнения: 
* Контекст БД;
* Имя таблицы;
* Данные для обновления записей; 
Поле заполняется в формате словаря.

Пример заполнения: 

  .. figure:: БД/БД17.png
         :alt:

* Первичный ключ. 

Пример заполнения параметров: 

  .. figure:: БД/БД18.png
         :alt:


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Удаление записи из базы данных  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Выбрать действие «Удалить запись». 

  .. figure:: БД/БД19.png
     :alt:


2. Заполнить параметры действия. Все поля обязательны для заполнения:
* Контекст БД;
* Имя таблицы;
* Первичный ключ;
Поле заполняется в формате словаря. 

Пример заполнения параметров:

  .. figure:: БД/БД20.png
         :alt:


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Закрытие транзакции   
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Выбрать действие «Закрыть транзакцию». 
   
   .. figure:: БД/БД21.png
         :alt:  

2. Заполнить параметры:
   
* Контекст БД
  
Пример заполнения параметров:

   .. figure:: БД/БД22.png
         :alt:  


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Откат транзакции   
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Выбрать действие «Откатить транзакцию». 

   .. figure:: БД/БД23.png
         :alt:  

Заполнить параметры: 
* Контекст БД
  
Пример заполнения параметров:

   .. figure:: БД/БД24.png
         :alt: 
