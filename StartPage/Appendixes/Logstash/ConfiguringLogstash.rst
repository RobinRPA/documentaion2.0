=====================================================
Конфигурирование Logstash после установки
=====================================================

.. note:: Предполагается что установка осуществлялась в соответствии с базовым руководством производителя.

Изменения вносятся в единственный файл **/etc/logstash/conf.d/base.conf**: 

- Секция **input:**

.. literalinclude:: base.conf
  :language: yaml
  :encoding: utf-8
  :lines: 1-7
  :linenos:


- Секция **filter:**

.. literalinclude:: base.conf
  :encoding: utf-8
  :lines: 9-50
  :linenos:


 
- Секция **output:**

.. literalinclude:: base.conf
  :language: yaml
  :encoding: utf-8
  :lines: 52-65
  :linenos:


.. note:: В зависимости от режима развертывания в секции hosts указывается либо ip/fqdn:port сервера elastic либо входная точка координатора кластера

