# This is a list of python packages used to generate documentation. This file is used with pip:
# pip install --user -r requirements.txt
#
sphinx==1.7.5
sphinx-rtd-theme==0.5.1
sphinxcontrib-blockdiag==1.5.5
sphinxcontrib-seqdiag==0.8.5
sphinxcontrib-actdiag==0.8.5
sphinxcontrib-nwdiag==0.9.5

