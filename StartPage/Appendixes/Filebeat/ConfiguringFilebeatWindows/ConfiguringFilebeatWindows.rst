=====================================================
Конфигурирование filebeat для Windows
=====================================================

.. note:: На примере версии **7.9.2**
  
  Далее в описании предполагается путь установки  **c:\\filebeat**


- В зависимости от разрядности ОС копируем содержимое архива **filebeat-7.9.2-windows-x86.zip** или **filebeat-7.9.2-windows-x86_64.zip** в директорию **c:\\filebeat**
- Создаем или редактируем файл **c:\\filebeat\\filebeat.yml** следующего содержания:

.. literalinclude:: ../filebeat.win.yml
  :language: yaml
  :encoding: utf-8
  :lines: 1-
  :linenos:

.. Warning:: В данной конфигурации подразумевается типовая установка без переназначения пути к пользовательским  профилям. В ином случае в конфигурации следует заменить:

    - **C:\\Users** - на соответствующий путь к пользовательским профилям.
    - **C:\\Windows\\System32\\config\\systemprofile** - на соответствующий путь к профилю системной учетной записи 


- выполняем powershell-скрипт с правами администратора

 .. code-block:: powershell

  cd c:/filebeat
  c:/filebeat> ./install-service-filebeat.ps1
  

.. note:: В случае получения сообщения о запрете выполнения Powershell-скриптов можно временно разрешить запуск PowerShell скриптов выполнив:

    **Set-ExecutionPolicy Bypass**

- Альтернативный вариант установки службы через sc.exe: 

 .. code-block:: bash

    sc.exe create filebeat binPath="c:/filebeat/filebeat.exe --environment=windows_service -c c:/filebeat/filebeat.yml --path.home c:/filebeat/ --path.data c:/filebeat/data --path.logs c:/filebeat/logs -E logging.files.redirect_stderr=true start=auto"
    sc.exe start filebeat

.. note:: Требуется запуск с правами администратора