import argparse, sys, re, threading, time, os, subprocess, shutil
import pathlib




def main(src, dst):
    print("Source: ", src)
    print("Destination: ", dst)
    extension = 'rst'

    paths=[]
    for dir_path, dir_names, file_names in os.walk(src):
        for file_name in [f for f in file_names if f.endswith("." + extension)]:
            paths.append((dir_path, file_name))
            #print(dir_path.replace(src,''), file_name)
            #s = len(pathlib.Path(src).parts)
            q = pathlib.Path(dir_path)
            d=os.path.join(dst,q.parts[-3],q.parts[-2])
            #print(dir_path,'       ',d)
            shutil.copytree(dir_path, d, ignore=shutil.ignore_patterns('.html','_._'))

def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--source_directory', help="files with helps rst")
    parser.add_argument('-d', '--destination_directory', help="Destination directory")
    return parser

if __name__ == "__main__":
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])
    if not namespace.source_directory:
        namespace.source_directory='../NugetPackages/Robin-Def'
    if not namespace.destination_directory:
        namespace.destination_directory='../_docs'
    main(namespace.source_directory,namespace.destination_directory)
