import argparse
import re
import os

from datetime import datetime
from pathlib import Path
from typing import List


def find_paths(root_path: Path, searchable_file_name: str) -> List[Path]:
    paths = []
    for dir_path, dir_names, file_names in os.walk(root_path):
        for file_name in [f for f in file_names if f == searchable_file_name]:
            paths.append(Path(dir_path).absolute().joinpath(Path(file_name)))
    return paths


def set_build_stamps(uiapps_branch_name: str, paths: List[Path]) -> None:
    is_release = bool(re.match(r"release_\d+\.\d+\.\d+", uiapps_branch_name))
    if is_release:
        release_version = re.findall(r"\d+\.\d+\.\d+", uiapps_branch_name)[0]
        doc_version = f"Release: {release_version}"
    else:
        doc_version = f"dev"
    build_stamp = f"{datetime.now().year} · {doc_version} · ROBIN RPA Team"
    print(f'Copyright для документации: "{build_stamp}"')
    for file_path in paths:
        with open(str(file_path), 'r+', encoding='utf-8') as f:
            content = f.read()
        content = re.sub(r"\d{4}, ROBIN RPA Team", build_stamp, content)
        with open(str(file_path), 'w', encoding='utf-8') as f:
            f.write(content)


def main() -> None:
    args = parse_args()
    root_path = Path(args.root_path).absolute()
    uiapps_branch_name = args.uiapps_branch_name
    paths = find_paths(root_path, 'conf.py')
    set_build_stamps(uiapps_branch_name, paths)


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--root_path', help='project root dir path', required=True, )
    parser.add_argument('-u', '--uiapps_branch_name', help='BuildUIApps branch name', required=True, )
    return parser.parse_args()


if __name__ == '__main__':
    main()
