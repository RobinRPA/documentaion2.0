import re
import sys

from pathlib import Path


def branch_name_parser():
    file = Path('uiapps_branch.txt')
    if not file.exists() or not file.is_file():
        sys.exit('Не удалось найти файл c именем ветки репозитория "BuildUIApps", с которой нужно подтянуть "release.xml". '
                 'Файл: "{file}"')
    with open(str(file), encoding='utf-8') as f:
        match = re.search(r'\b(BranchName:\s)([A-Za-z\d\.\\\/_\-]+)\b', f.read())
    if match and len(match.groups()) == 2:
        print(match.group(2), end='')
    else:
        sys.exit(f'В файле не найдено имя ветки репозитория "BuildUIApps", с которой нужно подтянуть "release.xml". '
                 f'Файл: "{file}"')
