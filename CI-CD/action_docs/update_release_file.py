import argparse
import base64
import requests

from lxml import etree
from pathlib import Path


UI_APPS_PROJECT_ID = 116
UI_APPS_RELEASE_FILE_PATH = "release.xml"


def validate_release_file(content: bytes) -> None:
    try:
        etree.fromstring(content)
    except Exception:
        print(f"Из репозитория BuildUIApps ({UI_APPS_PROJECT_ID=}) получен не валидный {UI_APPS_RELEASE_FILE_PATH}")
        raise


def update_release_file(file_path: Path, content: bytes) -> None:
    with open(str(file_path), 'wb') as f:
        f.write(content)
    print(f"{UI_APPS_RELEASE_FILE_PATH} обновлён")


def get_release_file_content(branch_name: str, token: str) -> bytes:
    url = f"http://robingit.itbs.it.ru/api/v4/projects/{UI_APPS_PROJECT_ID}/repository/files" \
          f"/{UI_APPS_RELEASE_FILE_PATH}?ref={branch_name}"
    headers = {"PRIVATE-TOKEN": token}
    req = requests.get(url, headers=headers)
    if req.status_code != 200:
        raise Exception(f"Не удалось получить содержимое релиз файла из BuildUIApps.\n"
                        f"Код возврата: {req.status_code}\n"
                        f"Ошибка: '{req.text}'\n"
                        f"{UI_APPS_RELEASE_FILE_PATH=}\n")
    req_data = req.json()
    encoded_content: str = req_data.get('content')
    content = base64.decodebytes(encoded_content.encode('utf-8'))
    return content


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--release_file_path', dest="release",  help="release.xml file local path", required=True, )
    parser.add_argument('-b', '--uiapps_branch_name', dest="branch", help="BuildUIApps branch name", required=True, )
    parser.add_argument('-t', '--private_api_token', dest="token", help="GitLab private 'api' token", required=True, )
    return parser.parse_args()


def main():
    args = parse_args()
    content = get_release_file_content(args.branch, args.token, )
    validate_release_file(content)
    update_release_file(Path(args.release), content, )


if __name__ == '__main__':
    main()
