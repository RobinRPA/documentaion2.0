import argparse

import update_super_actions_file

from pathlib import Path

from update_super_actions_file import validate_file, update_file, get_file_content


LIST_GROUPS_FILE_PATH = "ListGroups.xml"


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--list_groups_file_path', dest="list_groups",  help="ListGroups.xml file local path", required=True, )
    parser.add_argument('-b', '--uiapps_branch_name', dest="branch", help="BuildUIApps branch name", required=True, )
    parser.add_argument('-t', '--private_api_token', dest="token", help="GitLab private 'api' token", required=True, )
    return parser.parse_args()


def main():
    update_super_actions_file.LIST_ACTIONS_FILE_PATH = LIST_GROUPS_FILE_PATH
    args = parse_args()
    content = get_file_content(args.branch, args.token, )
    validate_file(content)
    update_file(Path(args.list_groups).absolute(), content, )


if __name__ == '__main__':
    main()
