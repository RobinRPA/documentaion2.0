import argparse
import base64
import requests

from lxml import etree
from pathlib import Path


UI_APPS_PROJECT_ID = 116
ACTION_LIST_PROJECT_ID = 267
UI_APPS_RELEASE_FILE_PATH = "release.xml"
LIST_ACTIONS_FILE_PATH = "ListActions.xml"


def validate_file(content: bytes) -> None:
    try:
        etree.fromstring(content)
    except Exception:
        print(f"Из саб-модуля 'actions-list' ({ACTION_LIST_PROJECT_ID=}) получен не валидный {LIST_ACTIONS_FILE_PATH}")
        raise


def update_file(file_path: Path, content: bytes) -> None:
    with open(str(file_path), 'wb') as f:
        f.write(content)
    print(f"{LIST_ACTIONS_FILE_PATH} обновлён")


def get_action_list_commit(ui_apps_branch_name: str, token: str, pagination_num: int = 100) -> str:
    """ Стучится в репу UIApps, узнаёт коммит сабмодуля actions-list и возвращает его в виде строки """
    url = f"http://robingit.itbs.it.ru/api/v4/projects/{UI_APPS_PROJECT_ID}/repository/tree?" \
          f"ref={ui_apps_branch_name}&per_page={pagination_num}"
    headers = {"PRIVATE-TOKEN": token}
    req = requests.get(url, headers=headers)
    if req.status_code != 200:
        raise Exception(f"Не удалось получить содержимое репозитория BuildUIApps.\n"
                        f"Код возврата: {req.status_code}\n"
                        f"Ошибка: '{req.text}'\n")
    req_data = req.json()
    actions_list_data = [i for i in req_data if i.get('path') == 'actions-list']
    if not actions_list_data:
        raise Exception("Не удалось найти сабмодуль 'actions-list' в репозитории BuildUIApps.\n"
                        f"Полученная дата: {req_data}")
    actions_list_data = actions_list_data[0]
    commit = actions_list_data.get('id')
    if not commit:
        raise Exception("Не удалось получить id коммита у сабмодуля 'actions-list' в репозитории BuildUIApps!")
    return commit


def get_file_content(branch_name: str, token: str) -> bytes:
    action_list_commit = get_action_list_commit(branch_name, token)
    url = f"http://robingit.itbs.it.ru/api/v4/projects/{ACTION_LIST_PROJECT_ID}/repository/files" \
          f"/{LIST_ACTIONS_FILE_PATH}?ref={action_list_commit}"
    headers = {"PRIVATE-TOKEN": token}
    req = requests.get(url, headers=headers)
    if req.status_code != 200:
        raise Exception(f"Не удалось получить содержимое релиз файла из BuildUIApps.\n"
                        f"Код возврата: {req.status_code}\n"
                        f"Ошибка: '{req.text}'\n"
                        f"{UI_APPS_RELEASE_FILE_PATH=}\n")
    req_data = req.json()
    encoded_content: str = req_data.get('content')
    content = base64.decodebytes(encoded_content.encode('utf-8'))
    return content


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--list_actions_file_path', dest="list_actions",  help="ListActions.xml file local path", required=True, )
    parser.add_argument('-b', '--uiapps_branch_name', dest="branch", help="BuildUIApps branch name", required=True, )
    parser.add_argument('-t', '--private_api_token', dest="token", help="GitLab private 'api' token", required=True, )
    return parser.parse_args()


def main():
    args = parse_args()
    content = get_file_content(args.branch, args.token, )
    validate_file(content)
    update_file(Path(args.list_actions).absolute(), content, )


if __name__ == '__main__':
    main()
