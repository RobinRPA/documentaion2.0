import argparse, sys, re, threading, time, os, subprocess
import xml.etree.ElementTree as ET


INTERNAL_NUGET_PACK_SOURCE = os.getenv("INTERNAL_PACK_SOURCE")
NEXUS_USERNAME = os.getenv("NEXUS_USERNAME")
NEXUS_PASSWORD = os.getenv("NEXUS_PASSWORD")
if not INTERNAL_NUGET_PACK_SOURCE:
    INTERNAL_NUGET_PACK_SOURCE = 'http://packages-dev.rpa-robin.ru/repository/nuget-local'
ErrorMessage = None


def define_nuget_dir(category):
    dirto = os.path.join(os.getcwd(), "NugetPackages/")
    if category == "EnginePackage":
        return dirto + "Robin-Imp"

    if category == "DefPackage":
        return dirto + "Robin-Def"

    if category == "ImpPackage":
        return dirto + "Robin-Imp"

    if category == "GroupPackage":
        return dirto + "Robin-Group"

    if category == "DefTypePackage":
        return dirto + "Types/Def"

    if category == "ExceptionPackage":
        return dirto + "Exceptions"
    # все что не классифицировалось - в отдельную кучку на всякий случай
    return dirto + "Others"


def install_nupkg(category, package):
    print('.', end='')
    global ErrorMessage
    if ErrorMessage:
        return
    dir_to = define_nuget_dir(category)
    pack_name = package.split(" ")[0]
    version = package.split(" ")
    if len(version) == 0:
        sys.exit("Отсутствует версия пакета!!")
    version = version[1]
    try:
        packed_dest = f"{dir_to}/{pack_name}-{version}.nupkg"
        unpacked_dest = f"{dir_to}/{pack_name}.{version}"
        execution_result = subprocess.call(f"mkdir -p {dir_to} && curl -sS -u {NEXUS_USERNAME}:{NEXUS_PASSWORD} "
                                           f"{INTERNAL_NUGET_PACK_SOURCE}/{pack_name}/{version} --output {packed_dest} "
                                           f"&& unzip -q {packed_dest} -d {unpacked_dest}", shell=True)
    except Exception as ex:
        if ErrorMessage:
            ErrorMessage+="install_nupkg: Проблема: " + package + " (" + category + ")\n"
        else:
            ErrorMessage="install_nupkg: Проблема: " + package + " (" + category + ")\n"
        ErrorMessage+=ex
        return
    if execution_result > 0:
        if ErrorMessage:
            ErrorMessage+="install_nupkg: Проблема: " + package + " (" + category + ")\n"
        else:
            ErrorMessage="install_nupkg: Проблема: " + package + " (" + category + ")\n"
        #sys.exit("install_nupkg false: Проблема: " + package + " (" + category + ")")


def install_common(pk):
    if pk[0]=="nupkg":
        install_nupkg(pk[1],pk[2])


def main(release_file, PARL):
    print("parsing ", release_file)
    tree = ET.parse(release_file)
    root = tree.getroot()
    jobs=[]
    for child in root:
        for nesting1 in child:
            attr = nesting1.attrib
            for nesting2 in nesting1:
                for nesting3 in nesting2:
                    if nesting3.attrib:
                        jobs.append((nesting3.attrib['packageType'],nesting3.tag,nesting3.attrib['packageName']))
                if nesting2.attrib:
                    jobs.append((nesting2.attrib['packageType'],nesting2.tag,nesting2.attrib['packageName']))
            if attr:
                jobs.append((nesting1.attrib['packageType'],nesting1.tag,nesting1.attrib['packageName']))

    thr = []
    for j in jobs:
        if j[1]=='DefPackage':
            thread = threading.Thread(target=install_common, args=(j,))
            thr.append(thread)

    print(f'{len(thr)} packages will be downloaded')

    start=0
    while start<len(thr) and not ErrorMessage:
        if start+PARL >= len(thr):
            PARL=len(thr)-start
        for i in range(start, start+PARL):
            thr[i].start()
        for i in range(start, start+PARL):
            if thr[i].is_alive():
                thr[i].join()
        start=start+PARL
        if PARL==0:
            break
        time.sleep(1)

    if ErrorMessage:
        print("Errors: "+ErrorMessage)
        sys.exit(2)


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--release_file', help="file with packages for release")
    parser.add_argument('-t', '--threads_num', help="Number of concurrent threads")
    return parser


if __name__ == "__main__":
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])
    if not namespace.threads_num:
        namespace.threads_num='16'
    try:
        PARL = int(namespace.threads_num)   # если PARL = 48  # 32-64 на 8 процессорах и 4 гб озу в пределах погрешности
    except ValueError:
        PARL = 4
        #sys.exit(f'Incorrect --threads_num argument value. Value: {namespace.threads_num}')
        print("Неверно указан или неуказана степень паралелизма. Устанавливаем в 4")
    if PARL < 0:
        PARL = 4 

    if not namespace.release_file:
        namespace.release_file = '../release.xml'
    main(namespace.release_file, PARL)
