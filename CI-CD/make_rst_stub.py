import os, argparse, sys


def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--prepare_directory', help="directory for prepare")
    return parser

if True:
    parser = create_parser()
    n = parser.parse_args(sys.argv[1:])
    if not n.prepare_directory:
        sys.exit("Please set directory for prepare")

    print("Creating stub .rst in ", n.prepare_directory)
    for s in os.listdir(n.prepare_directory):
        f=os.path.join(n.prepare_directory,s)
        if os.path.isdir(f):
            with open(f+'/index.rst','w') as rst:
                rst.write("=================================================================\n"
                +s+"\n=================================================================\n"
                + ".. toctree::\n"
                + "   :maxdepth: 1\n"
                + "   :glob:\n"
                +"\n"
                +"   */*\n"
                +"\n"+"\n")
    print('Done')